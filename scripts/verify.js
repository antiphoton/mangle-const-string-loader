const assert = require('assert');
const path = require('path');
const crypto = require('crypto');
const fs = require('fs');
const childProcess = require('child_process');

const calculateHash = function calculateHash(content) {
  const hash = crypto.createHash('sha256');
  hash.update(content);
  const result = hash.digest('hex');
  return result;
};

const dirSum = function dirSum(file) {
  const stat = fs.statSync(file);
  if (stat.isFile()) {
    const content = fs.readFileSync(file);
    return calculateHash(content);
  } else if (stat.isDirectory()) {
    const a = fs.readdirSync(file);
    a.sort();
    const content = a.map(s => `${s} ${dirSum(path.resolve(file, s))}`).join('\n');
    return calculateHash(content);
  } else {
    assert(false);
  }
};

const oldHash = dirSum('./build');

childProcess.execSync('yarn build');

const newHash = dirSum('./build');

assert(oldHash === newHash);

