const {
  resolve,
} = require('path');
const {
  execSync,
} = require('child_process');

const pkg = require('../package.json');

const {
  CI_COMMIT_SHA,
  ARCHIVE_TOKEN,
} = process.env;

try {
  const stdout = execSync(
    [
      'curl',
      `https://www.caoboxiao.com/npm/upload/${pkg.name}/${pkg.version}/${CI_COMMIT_SHA}.tar?token=${ARCHIVE_TOKEN}`,
      ...[ '--data-binary', `@${resolve(__dirname, '..', `${CI_COMMIT_SHA}.tar`)}` ],
      ...[ '-w', '"%{http_code}"' ],
      ...[ '-X', 'POST' ],
      ...[ '--header', '"Content-Type:application/octet-stream"' ],
      ...[ '-o', '/dev/null' ],
      '--silent',
    ].join(' '),
  );
  const statusCode = parseInt(stdout.toString(), 10);
  if (statusCode !== 200) {
    console.log(statusCode);
    throw statusCode;
  }
} catch (e) {
  throw 'curl error';
}

