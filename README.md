## Build

This package is deployed on [https://www.caoboxiao.com/npm].

To install

```bash
yarn add https://www.caoboxiao.com/npm/latest/mangle-const-string-loader
```

## Usage

### As a [webpack loader](https://webpack.js.org/configuration/module/#rule-use)

```javascript
{
  module: {
    rules: [
      include,
      loader: require.resolve('mangle-const-string-loader/build/webpackLoader.js'),
      options,
    ],
  },
}
```

### As a [rollup plugin](https://rollupjs.org/guide/en#using-plugins)

```javascript
import mangleConstStringPlugin from 'mangle-const-string-loader/build/rollupPlugin.js';

{
  plugins: [
    mangleConstStringPlugin({
      include,
      ...options,
    }),
  ],
}
```

## Options

[Definitions in typescript](src/Options.ts)

### `options.mangle`

`'fileName'` `'line'` `'hash'` `undefined`

### `options.wrap`

`'redux-actions'` `'redux-actions-with-meta'` `undefined`

