const assert = require('assert');
const compile = require('./compile.js');
const refuseToCompile = require('./refuseToCompile.js');

describe('loader options', async () => {
  test('unrecognized mangle option', async () => {
    await refuseToCompile(
      'input.txt',
      {
        mangle: 'not a valid mangle option',
      },
    );
  });
  test('unrecognized wrap option', async () => {
    await refuseToCompile(
      'input.txt',
      {
        mangle: 'not a valid wrap option',
      },
    );
  });
});

describe('raw string', async () => {
  test('no mangle', async () => {
    const output = await compile(
      'input.txt',
    );
    expect(output).toMatchSnapshot();
  });

  test('mangle by file id', async () => {
    const output = await compile(
      'input.txt',
      {
        mangle: 'fileName',
      },
    );
    expect(output).toMatchSnapshot();
  });

  test('mangle by hash', async () => {
    const output = await compile(
      'input.txt',
      {
        mangle: 'hash',
      },
    );
    expect(output).toMatchSnapshot();
  });

  test('mangle by line number', async () => {
    const output = await compile(
      'input.txt',
      {
        mangle: 'line',
      },
    );
    expect(output).toMatchSnapshot();
  });
});

describe('redux actions', async () => {
  test('refuse not to mangle', async () => {
    await refuseToCompile(
      'input.txt',
      {
        wrap: 'redux-actions',
      },
    );
  });
  test('refuse to mangle by line ', async () => {
    await refuseToCompile(
      'input.txt',
      {
        mangle: 'line',
        wrap: 'redux-actions',
      },
    );
  });
  test('mangle by file', async () => {
    const output = await compile(
      'input.txt',
      {
        mangle: 'fileName',
        wrap: 'redux-actions',
      },
    );
    expect(output).toMatchSnapshot();
  });
  test('mangle by hash', async () => {
    const output = await compile(
      'input.txt',
      {
        mangle: 'hash',
        wrap: 'redux-actions',
      },
    );
    expect(output).toMatchSnapshot();
  });
});

