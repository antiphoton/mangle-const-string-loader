const webpackCompiler = require('./webpack.js');
const rollupCompiler = require('./rollup.js');

const compilers = [
  {
    name: 'webpack',
    compiler: webpackCompiler,
  },
  {
    name: 'rollup',
    compiler: webpackCompiler,
  },
];

module.exports = async function refuseToCompile(inputFile, options) {
  await Promise.all(compilers.map(async ({ name, compiler }) => {
    let success;
    try {
      await compiler(inputFile, options);
      success = true;
    } catch(e) {
    };
    if (success) {
      throw `${name} did not fail`;
    }
  }));
};

