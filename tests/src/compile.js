const React = require('react');
const webpackCompiler = require('./webpack.js');
const rollupCompiler = require('./rollup.js');

module.exports = async function compile(inputFile, options) {
  const [
    webpackResult,
    rollupResult,
  ] = await Promise.all([
    webpackCompiler(inputFile, options),
    rollupCompiler(inputFile, options),
  ]);
  expect(webpackResult).toStrictEqual(rollupResult);
  return React.createElement('script', {}, webpackResult);
};

