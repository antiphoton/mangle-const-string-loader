const path = require('path');
const rollup = require('rollup');
const myPlugin = require('../../build/rollupPlugin.js');

require('../../build/rollupPlugin');

module.exports = async (inputFile, options = {}) => {
  const bundle = await rollup.rollup({
    input: path.resolve(__dirname, '..', 'data', inputFile),
    external: [
      'redux-actions',
    ],
    plugins: [
      myPlugin(options),
    ],
  });
  return bundle.modules[0].code;
};

