const path = require('path');
const webpack = require('webpack');
const memoryfs = require('memory-fs');

require('../../build/webpackLoader');

module.exports = (fixture, options = {}) => {
  const entry = `./${fixture}`;
  const compiler = webpack({
    context: path.resolve(__dirname, '..', 'data'),
    entry,
    output: {
      path: '/tmp',
      filename: 'bundle.js',
    },
    externals: {
      'redux-actions': 'ASDF',
    },
    module: {
      rules: [{
        test: /\.txt$/,
        use: {
          loader: path.resolve(__dirname, '..', '..', 'build', 'webpackLoader.js'),
          options,
        },
      }],
    },
  });

  compiler.outputFileSystem = new memoryfs();

  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      if (err) {
        reject(err);
      } else if (stats.hasErrors()) {
        reject(stats.compilation.errors);
      } else {
        const {
          modules,
        } = stats.toJson();
        const module = modules.find((module) => {
          return module.name === entry;
        });
        resolve(module.source);
      }
    });
  });
};
