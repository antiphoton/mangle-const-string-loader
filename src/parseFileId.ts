import {
  loader,
} from 'webpack';
import {
  interpolateName,
} from 'loader-utils';

export interface FileId {
  readonly folderName: string,
  readonly fileName: string,
};

const parseFileId = function parseFileId(resourcePath: string): FileId {
  const context = {
    resourcePath,
  } as loader.LoaderContext;
  const folderName = interpolateName(context, '[folder]', {});
  const fileName = interpolateName(context, '[name]', {});
  return {
    folderName,
    fileName,
  }
};

export default parseFileId;

