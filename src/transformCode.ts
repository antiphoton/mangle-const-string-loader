import {
  createHash,
} from 'crypto';
import {
  ok as assert,
} from 'assert';

import {
  FileId,
} from './parseFileId';
import {
  Options,
  MangleOptions,
  WrapOptions,
} from './Options';

const getHash = function getHash(
  content: string,
  encoding: 'hex' | 'base64',
  length: number,
): string {
  const hash = createHash('sha256');
  hash.update(content);
  return hash.digest(encoding).substr(0, length);
};

const transformCode = function transformCode(
  fileId: FileId,
  content: string,
  options: Options,
): string {
  const keyNames: string[] = content.split(/\s+/).map(line => line.trim()).filter(s => !!s);
  if (keyNames.length === 0) {
    return '';
  }
  const shufflePrefix = getHash(content, 'base64', 6);
  const a = keyNames.map((keyName: string) => {
    const hash = getHash(`${shufflePrefix}${fileId.folderName}${fileId.fileName}${keyName}`, 'base64', 8);
    return {
      keyName,
      hash,
    };
  });
  a.sort((x1, x2) => (x1.hash > x2.hash ? 1 : 0) - (x1.hash < x2.hash ? 1 : 0));
  const headerLines = [
    options.wrap === WrapOptions.ReduxActions && `import { createAction } from 'redux-actions';`,
  ].filter(x => !!x);
  const bodyLines = a.map((item, i: number) => {
    const {
      keyName,
      hash,
    } = item;
    let stringValue;
    switch (options.mangle) {
      case MangleOptions.File: {
        stringValue = `[${fileId.folderName}/${fileId.fileName}]${keyName}`;
        break;
      }
      case MangleOptions.Line: {
        stringValue = `${i}`;
        break;
      }
      case MangleOptions.Hash: {
        stringValue = `${hash}`;
        break;
      }
      case undefined: {
        stringValue = `${keyName}`;
        break;
      }
      default: {
        throw new Error('Unrecognized mangle options');
      }
    }
    switch (options.wrap) {
      case WrapOptions.ReduxActions: {
        assert(options.mangle === MangleOptions.File || options.mangle === MangleOptions.Hash);
        stringValue = `createAction(${JSON.stringify(stringValue)})`;
        break;
      }
      case undefined: {
        stringValue = JSON.stringify(stringValue);
        break;
      }
    }
    return `export const ${keyName} = ${stringValue};`;
  });
  return [
    ...headerLines,
    ...bodyLines,
  ].join('\n');
};

export default transformCode;

