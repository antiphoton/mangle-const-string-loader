import {
  getOptions,
} from 'loader-utils';

import parseFileId from './parseFileId';
import transformCode from './transformCode';

const loader = function loader(this: any, source: string): string {
  const options = getOptions(this);
  const fileId = parseFileId(this.resourcePath);
  return transformCode(
    fileId,
    source,
    options,
  );
};

export default loader;

