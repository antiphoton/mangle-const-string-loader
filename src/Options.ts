export enum MangleOptions {
  File = 'fileName',
  Line = 'line',
  Hash = 'hash',
}

export enum WrapOptions {
  ReduxActions = 'redux-actions',
}

export interface Options {
  readonly mangle?: MangleOptions,
  readonly wrap?: WrapOptions,
};

