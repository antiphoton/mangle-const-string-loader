import {
  Options,
} from './Options';
import parseFileId from './parseFileId';
import transformCode from './transformCode';

const createFilter = require("rollup-pluginutils").createFilter;

interface RollupOptions extends Options {
  include: string,
  exclude: string,
};

const plugin = function plugin(options: RollupOptions): object | undefined {
  const filter = createFilter(options.include, options.exclude);
  return {
    name: 'mangled-const-string-plugin',
    transform: (code: string, id: string) => {
      if (filter(id)) {
        const fileId = parseFileId(id);
        return {
          code: transformCode(
            fileId,
            code,
            options,
          ),
          map: {
            mappings: '',
          },
        };
      } else {
        return undefined;
      }
    },
  };
}

export default plugin;

