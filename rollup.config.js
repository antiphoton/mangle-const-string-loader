import {
  builtinModules,
} from 'module';

import typescript from 'rollup-plugin-typescript2'

import pkg from './package.json'

const entries = [
  'index',
  'webpackLoader',
  'rollupPlugin',
]

export default entries.map((entryName) => ({
  input: `src/${entryName}.ts`,
  output: [
    {
      file: `build/${entryName}.js`,
      format: 'cjs',
    },
  ],
  external: [
    ...builtinModules,
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
  ],
  plugins: [
    typescript({
      typescript: require('typescript'),
    }),
  ],
}));

